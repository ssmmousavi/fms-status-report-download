const excel = require('exceljs');
const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser')
const fetch = require('node-fetch');


const app = express();

app.use(bodyParser.json())

app.use(cors());
// -> Express RestAPIs
app.post("/download", function (req, res) {

  console.log('LOG',req.body,req.body.from);

  let workbook = new excel.Workbook(); //creating workbook
  let worksheet = workbook.addWorksheet('Customers'); //creating worksheet

  fetch('http://localhost:8000/reports/statuses',{
    method:'POST',
    body:JSON.stringify({
      'deviceId':req.body.deviceId,
      'from':req.body.from,
      'to':req.body.to
    }),
    headers:{
      'content-type':'application/json'
    }
  }).then(res=>{
    console.log(res);
    
    if(res.ok){
      console.log(res);
      
      return res.json()
    }
  }).then(data=>{

    console.log(data , 'data');
    

  const jsonCustomers = JSON.parse(JSON.stringify(data));
  // console.log(jsonCustomers);

  //  WorkSheet Header
  worksheet.columns = [
    { header: 'نام خودرو', key: 'deviceName', width: 10 },
    { header: 'نام راننده', key: 'driverName', width: 30 },
    { header: 'نام گروه', key: 'groupName', width: 30 },
    { header: 'سرعت', key: 'speed', width: 10, outlineLevel: 1 },
    { header: 'زمان', key:'jalaaliDate', width:10},
    { header: 'استارت', key:'ignition', width:10},
    { header: 'حرکت' , key :'motion', width:10},
    { header: 'محدوده عملیاتی', key:'geofencesName', width:10},
    { header: 'latitude', key:'latitude', width:10},
    { header: 'longitude', key:'longitude', width:10},
    { header:'ارتفاع', key:'altitude', width:20},
    { header: 'خروج از محدوده', key:'exitGeofences', width:10},
    { header: 'ورودی2', key:'di2', width:10},
    { header: 'مسافت', key:'distance', width:10},
    { header: 'مسافت طی شده کل', key:'totalDistance', width:20},
    { header: 'زمان روشن بودن موتور', key:'hours', width:30}

  ];

  // Add Array Rows
  worksheet.addRows(jsonCustomers);
  // console.log('uitytr', worksheet);


  res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  res.setHeader('Content-Disposition', 'attachment; filename=' + 'customer.xlsx');

  return workbook.xlsx.write(res)
    // return res.send('sdfghj')
    .then(function () {
      res.status(200).end();
    });
})
})

// Create a Server
let server = app.listen(8001, function () {
  let host = server.address().address
  let port = server.address().port

  console.log("App listening at http://%s:%s", host, port)
})
